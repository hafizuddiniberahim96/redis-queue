# stoQ Alert

A stoQ automation plugin for handling Redis fileinfo

#### Usage

Copy all the files into `$STOQ_HOME/plugins` directory.

```bash
$ cp /path/to/analyzer $STOQ_HOME/plugins
$ stoq install /path/to/plugin
```
