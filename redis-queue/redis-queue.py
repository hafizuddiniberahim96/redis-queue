#   Copyright 2014-present PUNCH Cyber Analytics Group
#
#   Licensed under the Apache License, Version 2.0 (the 'License');
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an 'AS IS' BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

"""
Overview
========

Interact with Redis server for queuing

"""
import time
import json
import redis
import asyncio
import os
from concurrent.futures import ProcessPoolExecutor
from asyncio import Queue
from watchgod import awatch
from typing import Dict, List, Optional

from stoq.helpers import dumps, StoqConfigParser
from stoq.exceptions import StoqPluginException
from stoq.plugins import ProviderPlugin
from stoq.data_classes import (
    StoqResponse,
    Payload,
    PayloadMeta,
    Request,
)


class RedisPlugin(ProviderPlugin):
    def __init__(self, config: StoqConfigParser) -> None:
        super().__init__(config)

        self.publish_archive = config.getboolean(
            'options', 'publish_archive', fallback=True
        )
    #--------------------redis options---------------------#
        self.meta = config.get('options', 'meta', fallback='This msg will always be')
        self.redis_host = config.get('options', 'redis_host', fallback='127.0.0.1')
        self.redis_port = config.getint('options', 'redis_port', fallback=6379)
        self.max_connections = config.getint('options', 'max_connections', fallback=15)
        self.redis_queue = config.get('options', 'redis_queue', fallback='stoq')
        self._connect()

    async def ingest(self, queue: Queue) -> None:
       self.log.info(f'Monitoring redis queue {self.redis_queue}')
       while True:
            msg = self.conn.blpop(self.redis_queue, timeout=1)
            if not msg:
                await asyncio.sleep(2)
                continue
            data = msg[1].decode()
            payload_meta = PayloadMeta(extra_data={'msg' : json.loads(data),'plugin' :'redis_queue'})
            await  queue.put(Payload(b'payload_results', payload_meta=payload_meta))

    # redis configuration setting
    def _connect(self):
        self.conn = redis.Redis(
            host=self.redis_host,
            port=self.redis_port,
            socket_keepalive=True,
            socket_timeout=300,
            connection_pool=redis.BlockingConnectionPool(
                max_connections=self.max_connections
            ),
        )


