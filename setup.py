from setuptools import setup, find_packages

setup (
        name="redis-queue",
        version="3.0.0",
        author="Hafizuddin Iberahim",
        url="",
        license="Apache License 2.0",
        description="Handle Redis fileinfo",
        packages=find_packages(),
        package_data={'redis-queue': ['redis-queue.stoq']},
        include_package_data=True,
)
